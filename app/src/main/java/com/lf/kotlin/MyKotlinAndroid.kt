package com.lf.kotlin

/**
 * @date: 2023/4/28
 */
class MyKotlinAndroid {

    /*

        1. Jetpack
                 1. 它是一套组件库。（说明它是由许多个不同的组件库构成，并不是一个单一的组件库）
                 2. 使用 Jetpack 可以帮助我们在不同的 Android 版本和不同的设备上，实现行为一致的工作代码。
                   （说明 Jetpack 可以轻松的处理由 Android 版本不一致和设备不同产生的差异性和兼容性问题）
                 3. 目前 Jetpack 一共有 85 个组件库，有些看着很熟悉，比如：viewPager、fragment、recyclerview 等等

        2. Jetpack Compose
            Jetpack Compose 是一个用于构建原生Android UI 的工具包，它基于声明式的编程模型，因此你可以简单地描述UI的外观，
            而Compose则负责其余的工作-当状态发生改变时，你的UI将自动更新。

        3. Material
             一个由谷歌官方维护的基于 Material design风格设计的UI组件
             Material 是Google 创建的一个设计系统，旨在帮助团队为 Android、iOS、Flutter 和 Web 构建高质量的数字体验。
             Material组件是一套UI样式标准 是用于创建用户界面的交互式构建基块，并包括一个内置状态系统，用于传达聚焦、选择、激活、错误、悬停、按下、拖动和禁用状态。组件库可用于 Android、iOS、Flutter 和 Web。

        4. Android KTX
                Android KTX 是包含在 Android Jetpack 及其他 Android 库中的一组 Kotlin 扩展程序。
                KTX 扩展程序可以为 Jetpack、Android 平台及其他 API 提供简洁的惯用 Kotlin 代码。为此，这些扩展程序利用了多种 Kotlin 语言功能，
                其中包括：
                    扩展函数
                    扩展属性
                    Lambda
                    命名参数
                    参数默认值
                    协程

     */
}
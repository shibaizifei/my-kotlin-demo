package com.lf.mykotlin.classes.data

/**
 * @date: 2022/12/29
 */
data class Book(var page: Int) {


    /*
        数据类的限制：
            1. 主构造函数至少包含一个参数；
            2. 主构造函数的所有参数必须标识为val或者var；
            3. 数据类不能为abstract、open、sealed或者inner；
            4. 数据类是可以实现接口的，如(序列化接口)，同时也是可以继承其他类的，如继承自一个密封类
     */

    lateinit var name: String;
}

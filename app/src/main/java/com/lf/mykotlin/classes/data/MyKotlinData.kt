package com.lf.mykotlin.classes.data

/**
 * @date: 2023/4/28
 */
class MyKotlinData {

    private fun testData() {

        // 1. 修改数据类 属性
        // Koltin要修改数据类的属性，则使用其独有的copy()函数。其作用就是：修改部分属性，但是保持其他不变
        var book = Book(200)
        val mNewBook = book.copy(page = 300)

    }
}
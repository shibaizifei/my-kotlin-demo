package com.lf.mykotlin.classes.person

open class Person { // 无参构造函数

    // 其中没有open注解，子类中是不能覆写该属性的
    open lateinit var name:String;

    var height: Int = 0;

    // 次级构造函数，this关键字指向当前类对象实例
    constructor(name: String) : super() {
        this.name = name;
    }

    // 次级构造函数
    constructor(name: String, height: Int) : this(name) {
        this.name = name;
        this.height = height;
    }


    // 其中没有open注解，子类中是不能覆写该方法的
    open fun eat() {

    }

    fun run() {

    }

    fun play() {

    }

}
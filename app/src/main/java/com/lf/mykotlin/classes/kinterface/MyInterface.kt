package com.lf.mykotlin.classes.kinterface


/*
    接口的实现：
    class 类名 ： 接口名{
        // 重写的接口函数、属性等
        ...
    }

 */
interface MyInterface {

    // 接口中的属性只能是抽象的，不允许初始化值，接口不会保存属性值，实现接口时，必须重写属性：
    var name:String

    fun eat()

    fun run() {

    }
}
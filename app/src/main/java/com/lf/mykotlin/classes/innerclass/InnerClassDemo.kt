package com.lf.mykotlin.classes.innerclass

/**
 * @date: 2023/1/6
 */
class InnerClassDemo {

    class Outer {
        val zero = 0;
        var one = 1;

        // 使用inner关键字声明内部类
        // 持有了outer类的对象引用
        inner class Inner {

            fun sum() {
                var sum = zero + one
            }
        }


        // 匿名内部类
        fun doRun() {
            Thread(object : Runnable {
                override fun run() {

                }
            }).start()
        }
    }
}
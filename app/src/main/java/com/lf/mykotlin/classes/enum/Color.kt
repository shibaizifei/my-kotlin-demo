package com.lf.mykotlin.classes.enum

/**
 * @date: 2022/12/28
 *
 * 枚举类的一个基本用法就是实现一个类型安全的枚举，枚举类中的每一个枚举常量都是一个对象，并且他们之间用逗号分隔
 *
 * 每个枚举常量都包含两个属性：name（枚举常量名）和ordinal（枚举常量位置）
 */
enum class Color {

    RED,
    BLACK,
    BLUE,
    GREEN,
    WHITE
}
package com.lf.mykotlin.classes.innerclass

/**
 *
 * 普通嵌套类
 * @date: 2023/1/5
 */
class NormalClassDemo {

    class Outer {
        private val zero = 0
        val one = 1

        class Nested {

            val tw = 2
            fun getTwo(): Int {
                return 2
            }

            class Three {
                val three = 3;

                fun getThre(): Int {
                    return three
                }
            }
        }
    }
}
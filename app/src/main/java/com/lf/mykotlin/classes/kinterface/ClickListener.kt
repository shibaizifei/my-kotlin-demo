package com.lf.mykotlin.classes.kinterface

/**
 * @author: shancheli
 * @date: 2022/12/28
 */
abstract class ClickListener {

    abstract var addr : String ;
    abstract val weight : Float ;

    abstract fun doSwim() ;

    fun doFly() {
        println("doFly")
    }

    fun doEach() {
        println("doEach")
    }
}
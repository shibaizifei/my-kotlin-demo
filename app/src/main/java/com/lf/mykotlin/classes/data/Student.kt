package com.lf.mykotlin.classes.data

/**
 * @date: 2023/4/28
 */
sealed class Student {
    data class Person(val num1 : Int, val num2 : Int) : Student()

    object Add : Student()   // 单例模式
    companion object Minus : Student() // 单例模式
}


/*
    1. 定义密封类的关键字：sealed
    2. 密封类是不能被实例化的
 */
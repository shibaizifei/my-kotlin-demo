package com.lf.mykotlin.classes.kinterface

class MyImplementation : MyInterface {

    /*
        class 类名 ： 接口名 {
            … //重写的接口函数、属性等
        }

     */

    override lateinit var name: String


    override fun eat() {

    }

    override fun run() {

    }

}
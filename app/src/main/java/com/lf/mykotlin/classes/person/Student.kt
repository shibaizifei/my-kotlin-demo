package com.lf.mykotlin.classes.person

class Student(text: String, age: Int) : Person(text) {

    // 如果子类要重新声明父类已经声明过的 属性，可以使用override关键字来重写相关的属性，在重写属性时应做 到类型的兼容。
    // 每个声明的属性可以被初始化或被具有get方法的属性 所覆盖，属性重写需要使用open关键字修饰。
    override lateinit var name: String


    /**次级构造函数**/
    // 如果子类没有主构造函数，则必须在每一个二级构造函数中用 super 关键字初始化基类，或者在代理另一个构造函数。
    // 初始化基类时，可以调用基类的不同构造方法。
    constructor(name:String,age:Int,no:String,score:Int):this(name, age) {
        println("-------继承类次级构造函数---------")
        println("学生名： ${name}")
        println("年龄： ${age}")
        println("学生号： ${no}")
        println("成绩： ${score}")
    }

     override fun eat() {

        // 在派生类中，可以使用super关键字来调用超类的函数与属性
        super.eat()

        super.height + 1;

    }
}
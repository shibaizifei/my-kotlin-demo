package com.lf.mykotlin.datatype

/**
 * @date: 2023/4/27
 *
 * 数据类型
 */
class MyKotlinDataType {

    /*
        1. 数字
                Byte=> 字节 => 8位
                Short => 短整型 => 16位
                Int => 整型 => 32位
                Long => 长整型 => 64位
                Float => 浮点型 => 32位
                Double => 双精度浮点型 => 64位
        2. 布尔
            Boolean
                true
                false
        3. 字符
            Char
        4. 数组
        5. 字符串

    */



    // 字符串模版
    fun testString() {

        // 模板字符串
        val value: Int = 5;
        val str: String = "$value";
        println(str); // 输出： 5

        // $用在表达式
        val g = 3;
        val h = 2;
        val sum = "g + h = ${g + h}"
        println(sum) // 输出： g + h = 5
    }
}
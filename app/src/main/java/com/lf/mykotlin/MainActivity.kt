package com.lf.mykotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.util.Log

class MainActivity : AppCompatActivity() {

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
    }

    fun testGroupBy() {

        val people = listOf(
            Person("Tom", 25, "USA"),
            Person("John", 30, "USA"),
            Person("Lisa", 20, "Canada"),
            Person("Mike", 35, "Canada")
        )

        val groupedPeople = people.groupBy {
            it.country
        }

        Log.d("kotlin----", groupedPeople.toString())

    }

    data class Person(val name: String, val age: Int, val country: String)

}
package com.lf.mykotlin.keyword

/**
 * @date: 2023/4/25
 */
class MyKeyword {

    /*
        1. lazy 延迟加载
            只有使用到 name 这个属性的时候，lazy 后面的 Lambda 才会执行，name 的值才会真正计算出来
     */
    private val name by lazy {
        getName2()
    }


    /*
        2. lateinit 是用来告诉编译器，height 这个变量后续会妥善处置的
     */
    private lateinit var height: String


    // 3. by委托模式
    // MyDelegate

    // 6. $操作符
    fun testString() {

        // 模板字符串
        val value: Int = 5;
        val str: String = "$value";
        println(str); // 输出： 5

        // $用在表达式
        val g = 3;
        val h = 2;
        val sum = "g + h = ${g + h}"
        println(sum) // 输出： g + h = 5
    }


    //  7. as? 安全转换操作符
    // 当转换不成功的时候 直接返回null
    fun testAs() {

        var a: Int = 2;
        var m: Int? = a as? Int;
    }

    // 8. is 和 !is 类型判断操作符
    fun testis(obj: Any): Int? {

        if (obj !is Int) {
            println("obj不是Int类型")
        }

        if (obj is String && obj.length > 0) //obj自动换成"String"类型
            return obj.length;
        return null;
    }

    // 9. 可空类型和不可空类型
    // 常规变量var 是不能为null的
    // 当一个引用可能为 null 值时, 对应的类型声明必须明确地标记为可为 null。
    fun testNull(obj: Any) {

        // a 不可能为空
        var a: String = "abc";

        var b: String? = null

        // 非空断言“!!” 使得可空类型对象可以调用成员方法或者属性（但遇见null，就会导致 空指针异常）
        var lena: Int = b!!.length;

        // “不安全的”转换操作符   如果转换是不可能的，转换操作符会抛出一个异常。因此，称为不安全的
        val x: String = obj as String

        // null 不能转换为 String 因该类型不是可空的。 如果 y 为空，上面的代码会抛出一个异常。 为了让这样的代码用于可空值，请在类型转换的右侧使用可空类型
        val x2: String? = obj as String?

        // 为了避免异常，可以使用安全转换操作符 as?，它可以在失败时返回 null：
        val x3: String? = obj as? String


        // 类型后面加?表示可为空
        // 可空引用操作符 ?.
        var age: String? = "23"
        var l = age?.length;

        //如果age为null 抛出空指针异常
        val ages = age!!.toInt()

        // 如果age为null 不做处理返回 null
        val ages1 = age?.toInt()

        // ?: 当age是null的时候，返回-1
        val ages2 = age?.toInt() ?: -1
    }

    fun getName2() : String {
        return "zhangsan"
    }
}
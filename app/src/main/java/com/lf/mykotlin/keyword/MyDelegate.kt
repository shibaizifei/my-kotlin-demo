package com.lf.mykotlin.delegate

import android.util.Log
import kotlin.properties.ReadWriteProperty
import kotlin.reflect.KProperty

/**
 * @date: 2023/5/11
 *
 * 1. 属性委托
 * 2. 类委托
 */

// 委托和代理、单例
// https://blog.51cto.com/u_14496797/5360017

// Delegates委托
//https://blog.csdn.net/eclipsexys/article/details/113209483


/**
 * 1. 属性委托
 *      一个类的某个属性值不是在类中直接进行定义，而是将其委托给一个代理类，从而实现对该类的属性统一管理
 *      val/var <属性名>: <类型> by <表达式>
 *
 *      类委托，委托的是接口中指定的方法，
 *      属性委托，委托的是属性的get、set方法，属性委托实际上就是将get、set方法的逻辑委托给一个单独的类来进行实现对于val属性来说，委托的是getValue方法，对于var属性来说，委托的是setValue和getValue方法）
 *
 */
fun testPropertyDelegate() {

    PropertyDelegate().delegateProp = "123"

}

class PropertyDelegate {

    var delegateProp by MyDelegate()
}


class MyDelegate : ReadWriteProperty<Any, String> {
    override fun setValue(thisRef: Any, property: KProperty<*>, value: String) {
        Log.d("xys", "MyDelegate set $value $thisRef ${property.name}")
    }

    override fun getValue(thisRef: Any, property: KProperty<*>): String {
        return "MyDelegate get $thisRef ${property.name}"
    }
}





/**
 * 2. 类委托
 */
fun testClassDelegate() {
    var father = SmallHeadFather()
    father.washing();
}



interface IWashBowl {
    fun washing()
}

// 单例对象
object BigHeadSon:IWashBowl {
    override fun washing(){
        println("我是大头儿子，我洗碗赚1块钱")
    }
}

// 交给代理对象去做接口中的事情
class SmallHeadFather:IWashBowl by BigHeadSon {

}
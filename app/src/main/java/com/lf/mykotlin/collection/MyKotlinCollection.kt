package com.lf.mykotlin.collection

class MyKotlinCollection {

    /*
        1. List MutableList
        2. Map  MutableMap
        3. Set  MutableSet
     */


    fun testSet() {
        val empty= emptySet<String>() //创建一个空集
        val lan= setOf("Kotlin","Java","Scale","Node") //创建一个只读集
        val mutable= mutableSetOf<String>() //创建一个可变集
        mutable.add("Kotlin")
    }

    fun testList() {
        val list = listOf<Int>(1,2,3)//创建一个只读的 List 集合
        val mlist= mutableListOf<Int>(1,2,3)//创建一个可变的 List 集合

        val items = listOf("apple", "banana", "kiwifruit")

        for (item in items) {
            println(item)
        }


        // 使用 in 操作符来判断集合内是否包含某实例。
        when {
            "orange" in items -> println("juicy")
            "apple" in items -> println("apple is fine too")
        }

        // 使用 lambda 表达式来过滤（filter）与映射（map）集合：
        val fruits = listOf("banana", "avocado", "apple", "kiwifruit")
        fruits
            .filter { it.startsWith("a") }
            .sortedBy { it }
            .map { it.uppercase() }
            .forEach { println(it) }
    }


    fun testMap() {

        // 声明可变Map
        var mapMutable = mutableMapOf(1 to "Kotlin", 2 to "Java", 3 to "Node")
        mapMutable.put(4,"JavaScript")


        val emptyMap= emptyMap<Int,String>()//创建一个空的 Map 集合

        val map= mapOf(1 to "one",2 to "two",3 to "three")//创建一个普通的Map 集合

        val mmap= mutableMapOf(1 to "one",2 to "two",3 to "three")//创建一个可变的 Map 集合

        val hashmap= hashMapOf(1 to "one",2 to "two",3 to "three")//创建一个 hashmap 集合

        val sortedmap= sortedMapOf(1 to "one",2 to "two",3 to "three")//创建一个 sortedmap 集合
    }
}
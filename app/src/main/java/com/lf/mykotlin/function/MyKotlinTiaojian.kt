package com.lf.mykotlin.function

class MyKotlinTiaojian {

    /*
        1. 条件判断 if else
        2. for语句
        3. while语句
        4. do..while语句
        5. when分支语句
        6. 跳转语句 break  continue return

     */

    // 1 条件语句if
    fun testIf() {
        var a = 3;
        var b = 4;
        if (a > b) a else b

        var numB: Int = if ( a > 2 ) 3 else 5  // 当a大于2时输出numB的值为3，反之为5
    }


    // 2. 循环语句 for
    // for(item in collection){循环体}
    fun testFor() {
        val items = listOf("apple", "banana", "kiwifruit")
        for (item in items) {
            println(item)
        }

        val items2 = listOf("apple", "banana", "kiwifruit")
        for (index in items2.indices) {
            println("item at $index is ${items[index]}")
        }

        // 循环5次，且步长为1的递增
        for (i in 0 until 5){
            print("i => $i \t")
        }

        // 循环5次，且步长为1的递减
        for (i in 15 downTo 11){
            print("i => $i \t")
        }

        // ..符号 大于等于n，小于等于m
        for (i in 20 .. 25){
            print("i => $i \t")
        }


        // 设置步长 step
        for (i in 10 until 16 step 2) {
            print("i => $i \t")
        }

        val array = arrayOf(2,'a',3,false,9)
        array.forEach { println(it) }
    }

    // 3. 条件语句 when
    fun testWhen() {
        val x = 1
        val validNumbers = arrayOf(1, 2, 3)
        when (x) {
            in 1..10 -> print("x is in the range")
            in validNumbers -> print("x is valid")
            !in 10..20 -> print("x is outside the range")
            else -> print("none of the above")
        }

        println(describe(1))
        println(describe("Hello"))
        println(describe(1000L))
        println(describe(2))
        println(describe("other"))

        // when 也可以用来取代 if-else if 链。 如果不提供参数，
        // 所有的分支条件都是简单的布尔表达式，而当一个分支的条件为真时则执行该分支
        when {
            x.equals(2) -> print("x is odd")
            x.equals(3) -> print("y is even")
            else -> print("x+y is odd")
        }

        var num:Int = 5
        when(num > 5){
            true -> println("num > 5")
            false -> println("num < 5")
            else -> {
                println("error!")
                println("num = 5")
            }
        }


    }



    // 4 循环语句 while
    fun testWhile() {
        val items = listOf("apple", "banana", "kiwifruit")
        var index = 0
        while (index < items.size) {
            println("item at $index is ${items[index]}")
            index++
        }
    }

    // 5 循环语句 do while
    // 先执行一次循环体，然后再判断条件表达式，如果条件表达式的值为 true，则继续执行；否则跳出循环。
    // 也就是说，do...while循环中的循环体至少被执行一次
    fun testDoWhile() {
        var limit=100
        var sum =0
        var i=1
        do {
            sum+=i
            i++
        } while (i<limit)
        println(sum)
    }


    fun describe(obj: Any): String =
        when (obj) {
            1          -> "One"
            "Hello"    -> "Greeting"
            is Long    -> "Long"
            !is String -> "Not a string"
            else       -> "Unknown"
        }


    // 6. 使用 in 操作符来检测某个数字是否在指定区间内。
    fun testRange1() {
        val x = 10
        val y = 9
        if (x in 1..y+1) {
            println("fits in range")
        }

        for (i in 1..4) print(i) // 输出“1234”

        for (i in 4..1) print(i) // 什么都不输出

        var index = 3;
        if (index in 1..10) { // 等同于 1 <= i && i <= 10
            println(index)
        }

        // 使用 step 指定步长
        for (i in 1..4 step 2) print(i) // 输出“13”

        for (i in 4 downTo 1 step 2) print(i) // 输出“42”

        // 使用 until 函数排除结束元素
        for (i in 1 until 10) {   // i in [1, 10) 排除了 10
            println(i)
        }
    }

    // 检测某个数字是否在指定区间外。
    fun testRange2() {
        val list = listOf("a", "b", "c")

        if (-1 !in 0..list.lastIndex) {
            println("-1 is out of range")
        }
        if (list.size !in list.indices) {
            println("list size is out of valid list indices range, too")
        }
    }

    // 区间迭代
    fun testRange3() {
        for (x in 1..5) {
            print(x)
        }

        // 数列迭代。
        for (x in 1..10 step 2) {
            print(x)
        }
        println()
        for (x in 9 downTo 0 step 3) {
            print(x)
        }
    }

}
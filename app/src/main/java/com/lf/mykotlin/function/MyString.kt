package com.lf.mykotlin.function

/**
 * @date: 2023/4/27
 */
class MyString {

    /*
        1. 字符串查找
        2. 字符串截取
        3. 字符串替换
        4. 字符串分割
     */

    val str = "kotlin very good"


    // 获取第一个元素
    fun getFirst() {
        // 如果字符串为空串时，会抛出NoSuchElementException异常
        str.first()
        str[0]
        str.get(0)

        // 如果字符串为空串时，会返回null
        str.firstOrNull()
    }


    // 获取最后一个元素
    fun getLast() {
        // 如果字符串为空串时，会抛出NoSuchElementException异常
        // 其中 lastIndex 是一个拓展属性，其实现是 length - 1
        str.last()
//        str.get(lastIndex)
//        str[lastIndex]

        // 如果字符串为空串时，会返回null
        str.lastOrNull()
    }

}
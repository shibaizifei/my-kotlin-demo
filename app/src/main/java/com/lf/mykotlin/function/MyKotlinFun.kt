package com.lf.mykotlin.function

import com.lf.mykotlin.classes.person.Student
import java.util.concurrent.locks.Lock

class MyKotlinFun {

    // 1. 函数参数使用 Pascal 表示法定义——name: type。参数用逗号隔开， 每个参数必须有显式类型
    fun powerOf(number: Int, exponent: Int) {

        /*...*/
    }

    // 2. 函数参数可以有默认值，当省略相应的参数时使用默认值  这可以减少重载数量
    fun read( b: ByteArray, off: Int = 0, len: Int = b.size) {

    }

    // 3. 可变长度的参数方式
    fun vars(vararg v:Int) {
        for(vt in v) {
            print(vt)
        }
    }

//    vars(1, 2, 3, 4, 5)


    // 4. 当函数返回单个表达式时，可以省略花括号并且在 = 符号之后指定代码体即可：
    fun double(x: Int): Int = x * 2


    // 5. 当返回值类型可由编译器推断时，显式声明返回类型是可选的：
    fun double2(x: Int) = x * 2



    /*
        6. 函数的扩展方法
        给String类 扩展了一个 firstChar() 方法
     */
    fun String.firstChar(): String {
        if (this.length == 0) {
            return "";
        }

        return this[0].toString();
    }

    fun testExtension() {
        var str = "abc";
        var firstStr = str.firstChar();
    }


    // 6. 泛型函数
    fun <Person>testFanxing(p : Person): List<Person> {
        val pw = Student("zhangsan", 12)
        return listOf(p)
    }

    // 7. 高阶函数
    // 高阶函数（Higher Order Function）是一种特殊的函数，它把函数作为参数，或者返回一个函数
    fun <T> lock(lock: Lock, body: () -> T): T {
        lock.lock()
        try {
            return body()
        } finally {
            lock.unlock()
        }
    }

}
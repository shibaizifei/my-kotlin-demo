package com.lf.mykotlin.property

import com.google.android.material.tabs.TabLayout

/**
 * @date: 2023/4/27
 */
class MyKotlinProperty {

    //立即初始化
    var a: Int = 10

    //推导出类型
    var b = 5

    // 声明可空变量
    var c : Int? = 0
    val d : Int? = null



    /*
        1. lateinit关键字
            必须是可读且可写的变量，即用var声明的变量
            不能声明于可空变量。
            不能声明于基本数据类型变量。例：Int、Float、Double等，注意：String类型是可以的。
            声明后，在使用该变量前必须赋值，不然会抛出UninitializedPropertyAccessException异常。
     */
    private lateinit var mTabLayout : TabLayout


    /*
        2. lazy 延迟初始化
                指当程序在第一次使用到这个变量（属性）的时候在初始化
                使用lazy{}高阶函数，不能用于类型推断。且该函数在变量的数据类型后面，用by链接。
                必须是只读变量，即用val声明的变量
     */
    private val mStr : String by lazy {
        "我是延迟初始化字符串变量"
    }


    /*
        3. 常量
            const只能修饰val，不能修饰var

     */
//    const val NUM_A : String = "顶层声明"



    // 1. 定义变量
    fun testProperty() {

        // 1. 变量分为 var (可变的) 和 val (不可变的)。
        // val 对应的是final类型
        val a = "hello a!" //不可变
        println(a) //输出hello a!

        var b = "hello " //可变
        b = "hello b!"
        println(b) //输出hello b!

        // 如果没有初始值 类型不能省略
        val c: Int;

        // 只读变量
        val index2: Int = 3;

        // 省略类型
        var name2 = "lisi"
        var index3 = 6

    }

    // 2. 字符串模板
    // 模板表达式以美元符（$）开头，要么由一个的名称构成  要么是用花括号括起来的表达式:
    fun testString2() {

        var name = "zhangsan"

        println("index is $name")
        println("index length is ${name.length}")
    }
}
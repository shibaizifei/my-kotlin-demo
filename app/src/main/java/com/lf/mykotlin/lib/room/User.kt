package com.lf.mykotlin.lib.room

import androidx.room.ColumnInfo
import androidx.room.Entity
import androidx.room.PrimaryKey

/**
 * @date: 2023/5/12
 *
 * Android Room 数据实体类详解:
 * https://blog.csdn.net/yingaizhu/article/details/117573604
 *
 *
 * 1. Room 数据实体类定义
 *      定义 Room 数据实体类，使用 data class 关键字，并使用 @Entity 注解标注
 *
 *
 * 2. 指定数据实体类对应的表名
 *      Room 会根据实体类的类为表名（在数据库中表名其实不区分大小写），
 *      开发者也可以在 @Entity 注解通过 tableName 参数指定表名，这样 Room 在创建表的时候就是以指定的名称命名
 *      注意事项：数据库表的名称是不区分大小写的
 *
 *
 * 3. 设定数据表主键
 *      每一个数据实体类必须定义一个主键，确保在数据表中每一行数据唯一。可以指定一个或者多个列作为表主键，
 *      设置主键可以在定义数据实体类属性时使用 @PrimaryKey 注解标示（指定单个列为主键建议使用此方法），
 *      也可以在定义数据实体类时在 @Entity 注解中通过 primaryKeys 属性声明（指定多个列组合为主键建议使用此方法，
 *      @Entity 注解的 primaryKeys 属性是一个数组）
 *
 * 4. 指定表的列名称
 *      默认情况下，Room 会根据数据实体类的属性名作为对应列的名称，如果开发者想指定不同的名称，可以使用 @ColumnInfo 注解的 name 属性指定列名称
 *
 * 5. 忽略属性
 *      使用 @Ignore 注解标示该属性即可，这样一来，即使 Room 能够访问该属性，也会忽略该属性，不会作为数据表中的一列
 */

@Entity(tableName = "users")
data class User(@PrimaryKey val uid: Int,
                val name: String,
                val age: Int
                )


// 在数据实体类的属性使用 @PrimaryKey 注解声明主键
@Entity(tableName = "users")
data class User2(@PrimaryKey val uid: Int, val name: String, var age: Int)

// 在 `@Entity` 注解中通过 `primaryKeys` 属性声明主键
@Entity(tableName = "users", primaryKeys = ["uid"])
data class User3(val uid: Int, val name: String, val age: Int)


/**
 * 如果指定单个列为主键，还可以设置主键值自动递增，这样一来在实例化数据实体类对象时就无需指定数据实体类中主键的值。
 * 通过将 @PrimaryKey 注解的 autoGenerate 属性值设置为 true 即可，
 * 但是要注意的是，如果设置为主键值递增，那么在实例化时主键属性就不要赋值，在构造方法中将主键参数设置可为空即可（这样在实例化时主键参数传入null，由数据库实现自增）
 */
// 自增主键  实例化时，主键参数传入null即可
@Entity(tableName = "users")
data class User4(@PrimaryKey(autoGenerate = true) var uid: Int?, @ColumnInfo(name = "name") val name: String, val age: Int)

fun testRoom4() {

}

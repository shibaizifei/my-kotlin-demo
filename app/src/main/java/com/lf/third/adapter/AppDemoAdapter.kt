package com.lf.third.adapter

import com.chad.library.adapter.base.BaseQuickAdapter
import com.chad.library.adapter.base.viewholder.BaseViewHolder
import com.lf.mykotlin.classes.person.Person

/**
 * @date: 2023/1/4
 */
class AppDemoAdapter(layoutResId: Int, data: MutableList<Person>?) :
    BaseQuickAdapter<Person, BaseViewHolder>(layoutResId, data) {


    override fun convert(holder: BaseViewHolder, item: Person) {
        holder.layoutPosition
    }
}